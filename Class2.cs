﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Plugin2 : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var service = factory.CreateOrganizationService(context.UserId);

            if (context.InputParameters.Contains("Target") &&
                       context.InputParameters["Target"] is Entity)
            {

                if (context.Stage == 40 && context.MessageName == "Update")
                {
                    var recordData = (Entity)context.InputParameters["target"];
                    var preImageData = (Entity)context.PreEntityImages["Image"];

                    var champ1 = recordData.Contains("new_champ1") ? recordData["new_champ1"] : preImageData["new_champ1"];
                    var champ2 = recordData.Contains("new_champ2") ? recordData["new_champ2"] : preImageData["new_champ2"];

                    if (champ1 != champ2)
                    {
                        throw new InvalidPluginExecutionException("Les champs 1 et 2 sont différents!");
                    }

                }
            }
        }
    }
}
