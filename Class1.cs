﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Plugin1 : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            var factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            var service = factory.CreateOrganizationService(context.UserId);

            if (context.Stage == 20 && context.MessageName == "create")
            {
                var recordData = (Entity)context.InputParameters["target"];
                var name = $"{recordData["new_champ1"]} - {recordData["new_champ2"]}";
                recordData["new_name"] = name;
            }
        }
    }
}
